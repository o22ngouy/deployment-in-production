# Déploiement de App-vapormap avec Terraform et Ansible
---

## Descriptif du projet
Ce projet a pour but d'automatiser le déploiement de l'application app-vapormap avec d'abord Terraform pour créer les ressources et Ansible pour le déploiement de l'application. 3 instances sont créees, mais l'application n' est déployée que sur une seule instance.
```
                                        --------- 
client <----------------------> 8000   | NODE 02 |
                                        ---------
                                       | NODE 01 |       
                                        ---------
                                           |
                       ---------           |           
Admin  <---------> 22 | BASTION | ---------+        
                       ---------   

```
## Déploiement avec CICD

## prérequis avant  utilisation du  dépôt

Pour utiliser la chaîne CI/CD de ce dépôt, il faut au préalable avoir :

* Un runner Gitlab avec un exécutor shell (sous ubuntu)
* Terraform et Python, python-venv installés sur le Runner
* Utilise Openstack comme Cloud Provider

## Utilisation  du dépôt

Pour utiliser le projet, il faut:

* Configurer les variables dans le fichier main.tf  pour le déploiement de l'infra avec Terraform.
```terraform
module "principal" {
    source = "./modules"
    external_network = "external"
    router_name = "router_tf_fr"
    network_name = "network_tf_fr"
    subnet_name = "subnet_tf_fr"
    subnet_ip_range = "192.168.61.0/24"
    group_bastion = "secgroup_bastion"
    secgroup_app = "secgroup_application"
    name_instance_bastion = "bastion"
    key_pair = "demo" # nom de votre clé
    key_pair_2 = "key" # nom seconde clé générée par terraform
    secgroup_internal = "secgroup_internal"
    dns = ["192.44.75.10"]
    path_key_pair = "/home/user/Téléchargements/demo.pem" # emplacement complet de votre clé privée sur la machine hébergeant terraform
    path_key_pair_2 = "/home/ubuntu/.ssh" # emplacement sur bastion où copié  la seconde clé générée par terraform (sans nom de la clé)
    path_key_pair_2_local = "/home/user/.ssh" # emplacement local(machine hébergeant terraform) où sera la copie de la seconde clé (sans nom de la clé)
}
```
* Créer un fichier d'environnement avec le nom "**env**" dans Gitlab  pour charger les variables d'environnement de votre fichier openrc.sh, comme ceci:
```bash
 
export OS_AUTH_URL=##
export OS_PROJECT_ID=##
export OS_PROJECT_NAME=##
export OS_USER_DOMAIN_NAME=##
if [ -z "$OS_USER_DOMAIN_NAME" ]; then unset OS_USER_DOMAIN_NAME; fi
export OS_PROJECT_DOMAIN_ID="default"
if [ -z "$OS_PROJECT_DOMAIN_ID" ]; then unset OS_PROJECT_DOMAIN_ID; fi
# unset v2.0 items in case set
unset OS_TENANT_ID
unset OS_TENANT_NAME
# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_USERNAME=##
# password
export OS_PASSWORD=##
# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME=##
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3
```
* Configurer les variables d'environnement pour  personnaliser l'hôte sur lequel le déploiement s'effectuera , dans le fichier .gitlab-ci.yml.

```bash
ANSIBLE_USER=ubuntu # nom user des instances 
ANSIBLE_BASTION_PRIVATE_KEY=/home/user/Téléchargements/demo.pem # clé privée utilisateur 
KEY_PATH_2=/home/user/.ssh/key.pem # seconde clé générée par terraform
```
* La pipeline n'est déclenchée que si le message de commit est "**prod**".

## Test de l'application

Pour tester l'application, il suffit d'entrer l'adresse Ip publique  de l'instance "**node02**" sur le port **8000** dans un navigateur :
```bash
adresse_ip_publique_node02:8000
```
## Déploiement sans CICD
#### Déploiement de l'Infra 
* Récupération du dépôt
```
git clone https://gitlab.imt-atlantique.fr/o22ngouy/deployment-in-production.git

cd deployment-in-production
```
* Changez les variables dans le fichier main.tf comme vu précédemment.
* Sourcer votre fichier rc.
* Et enfin exécutez les commandes pour initialiser le projet Terraform et appliquer la création des ressources.

### Déploiement de l'application
* Toujours dans le dossier deployment-in-production
* Ensuite exécutez les commandes suivantes Tout en remplaçant les valeurs des variables d'environnement si nécessaire :
```bash
### Création de l'environnement virtuel
python3 -m venv ./venv/ansible 
### Déclaration des variables de configuration de l'inventory
export ANSIBLE_BASTION_IP=$(cat ip_public_nodes.txt | grep bastion | awk '{print $2}')
export NODE_02_IP=$(cat ip_private_nodes.txt | grep node02 | awk '{print $2}')
export NODE_02_IP_PUBLIC=$(cat ip_public_nodes.txt | grep node02 | awk '{print $2}')
export ANSIBLE_USER=ubuntu # nom user des instances distantes
export ANSIBLE_BASTION_PRIVATE_KEY=/home/user/Téléchargements/demo.pem # clé privée utilisateur 
export KEY_PATH_2=/home/user/.ssh/key.pem ## seconde clé privée générée par terraform
### Activation de l'environnement virtuel
source ./venv/ansible/bin/activate
pip install ansible ansible-core
### Création du fichier d'inventory
envsubst '${NODE_02_IP},${NODE_02_IP_PUBLIC},${ANSIBLE_BASTION_IP},${KEY_PATH_2},${ANSIBLE_USER},${ANSIBLE_BASTION_PRIVATE_KEY}' < host.conf.template > ./ansible_file/host.ini
cd ansible_file
chmod 600 ${KEY_PATH_2}
ansible-playbook -i host.ini deploy.yml
deactivate
```
* Faites attention aux commentaires

### Test de l'application

Pour tester l'application, il suffit d'entrer l'adresse Ip publique  de l'instance "**node02**" sur le port **8000** dans un navigateur :
```bash
adresse_ip_publique_node02:8000
```